#pragma once

#include <vector>
#include "point.h"
#include "canvas.h"
#include "interpolated_triangle.h"
#include "triangle.h"

class vertex_array
{
public:
	vertex_array(int width, int height, int nx, int ny);
	~vertex_array();

	void draw(canvas &buffer);
	std::vector<point> get_point_buffer();
	std::vector<int> get_index_buffer();
	void set_point_buffer(std::vector<point>&);
	void set_index_buffer(std::vector<int>&);

private:
	int width_;
	int height_;

	int nx_;
	int ny_;

	std::vector<point> point_buffer;
	std::vector<int> index_buffer;

	std::vector<point> p_vector;

	void generate_index_buffer();
	void generate_point_buffer();
};

