#pragma once

#include <array>

struct color
{
	uint8_t r = 0;
	uint8_t g = 0;
	uint8_t b = 0;

	void set_color(uint8_t r, uint8_t g, uint8_t b);
};

