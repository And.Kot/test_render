#pragma once

#include "point.h"

class point;

struct uniforms
{
    int f0 = 0;
    int f1 = 0;
    int f2 = 0;
};

class gfx_program
{
public:
    virtual void set_uniforms(const uniforms&) = 0;
    virtual point point_shader(point&)         = 0;
    virtual color pixel_shader(point&)         = 0;
};

