#pragma once

#include "gfx_program.h"
#include <vector>

class my_gfx_program: public gfx_program
{
public:
	void set_uniforms(const uniforms&) override;
	point point_shader(point& p) override;
	color pixel_shader(point& p) override;

	std::vector<point> calculate_array(std::vector<point>&);

private:
	float mouse_x = 0;
	float mouse_y = 0;
	float mouse_r = 0;
};

