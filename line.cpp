#include "line.h"
#include <cmath>

line::line(point& p0, point& p1):
	p0_(p0),
	p1_(p1)
{
	fill(p0_, p1_, p_vector);
}

void line::draw(canvas& buffer)
{
	for (size_t i = 0; i < p_vector.size(); i++)
	{
		*(buffer.begin() + p_vector.at(i).get_x() + p_vector.at(i).get_y() * image_width) 
			= color(this->p_vector.at(i));
	}
}

void line::fill(point& p0_, point& p1_, std::vector<point>& p_vector)
{
	int x0 = p0_.get_x();
	int y0 = p0_.get_y();

	int x1 = p1_.get_x();
	int y1 = p1_.get_y();

	color c0 = p0_;
	color c1 = p1_;

	int r0 = c0.r;
	int g0 = c0.g;
	int b0 = c0.b;

	int temp;

	int r1 = c1.r;
	int g1 = c1.g;
	int b1 = c1.b;

	int d_r = int(c1.r) - int(c0.r);
	int d_g = int(c1.g) - int(c0.g);
	int d_b = int(c1.b) - int(c0.b);

	float delta_l_temp;
	float delta_l = std::sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));
	float k_r = d_r / delta_l;
	float k_g = d_g / delta_l;
	float k_b = d_b / delta_l;

	auto plot_line_low = [&](int x0, int y0, int x1, int y1) {
		int dx = x1 - x0;
		int dy = y1 - y0;
		int yi = 1;
		if (dy < 0)
		{
			yi = -1;
			dy = -dy;
		}
		int D = 2 * dy - dx;
		int y = y0;

		for (int x = x0; x <= x1; ++x)
		{
			delta_l_temp = delta_l - sqrt((x1 - x)*(x1 - x) + (y1 - y)*(y1 - y));
			p_vector.push_back(point(x, y,
				{ static_cast<uint8_t>(r0 + k_r * delta_l_temp),
				static_cast<uint8_t>(g0 + k_g * delta_l_temp),
				static_cast<uint8_t>(b0 + k_b * delta_l_temp) }));

			if (D > 0)
			{
				y += yi;
				D -= 2 * dx;
			}
			D += 2 * dy;		
		}
	};

	auto plot_line_high = [&](int x0, int y0, int x1, int y1) {
		int dx = x1 - x0;
		int dy = y1 - y0;
		int xi = 1;
		if (dx < 0)
		{
			xi = -1;
			dx = -dx;
		}
		int D = 2 * dx - dy;
		int x = x0;

		for (int y = y0; y <= y1; ++y)
		{
			delta_l_temp = delta_l - sqrt((x1 - x)*(x1 - x) + (y1 - y)*(y1 - y));
			p_vector.push_back(point(x, y,
				{ static_cast<uint8_t>(r0 + k_r * delta_l_temp),
				static_cast<uint8_t>(g0 + k_g * delta_l_temp),
				static_cast<uint8_t>(b0 + k_b * delta_l_temp) }));

			if (D > 0)
			{
				x += xi;
				D -= 2 * dy;
			}
			D += 2 * dx;	
		}
	};

	if (abs(y1 - y0) < abs(x1 - x0))
	{
		if (x0 > x1)
		{
			k_r = -k_r;
			k_g = -k_g;
			k_b = -k_b;

			temp = r1;
			r1 = r0;
			r0 = temp;

			temp = g1;
			g1 = g0;
			g0 = temp;

			temp = b1;
			b1 = b0;
			b0 = temp;

			plot_line_low(x1, y1, x0, y0);
		}
		else
		{
			plot_line_low(x0, y0, x1, y1);
		}
	}
	else
	{
		if (y0 > y1)
		{
			k_r = -k_r;
			k_g = -k_g;
			k_b = -k_b;

			temp = r1;
			r1 = r0;
			r0 = temp;

			temp = g1;
			g1 = g0;
			g0 = temp;

			temp = b1;
			b1 = b0;
			b0 = temp;

			plot_line_high(x1, y1, x0, y0);
		}
		else
		{
			plot_line_high(x0, y0, x1, y1);
		}
	}
}
