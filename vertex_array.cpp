#include "vertex_array.h"



vertex_array::vertex_array(int width, int height, int nx, int ny):
	width_(width),
	height_(height),
	nx_(nx),
	ny_(ny)
{
	generate_index_buffer();
	generate_point_buffer();

	for (int i = 0; i < index_buffer.size() - 2; i+=3)
	{
		//interpolated_triangle t(point_buffer.at)
		triangle::fill(point_buffer.at(index_buffer.at(i    )),
					   point_buffer.at(index_buffer.at(i + 1)),
			           point_buffer.at(index_buffer.at(i + 2)),
			           p_vector);
	}
}

vertex_array::~vertex_array()
{
}

void vertex_array::draw(canvas& buffer)
{
	for (size_t i = 0; i < p_vector.size(); i++)
	{
		*(buffer.begin() + p_vector.at(i).get_x() + p_vector.at(i).get_y() * image_width)
			= color(this->p_vector.at(i));
	}
}

std::vector<point> vertex_array::get_point_buffer()
{
	return point_buffer;
}

std::vector<int> vertex_array::get_index_buffer()
{
	return index_buffer;
}

void vertex_array::set_point_buffer(std::vector<point>& point_buffer)
{
	this->point_buffer = point_buffer;
	p_vector.clear();
	for (int i = 0; i < index_buffer.size() - 2; i += 3)
	{
		//interpolated_triangle t(point_buffer.at)
		triangle::fill(point_buffer.at(index_buffer.at(i)),
			point_buffer.at(index_buffer.at(i + 1)),
			point_buffer.at(index_buffer.at(i + 2)),
			p_vector);
	}
}

void vertex_array::set_index_buffer(std::vector<int>& index_buffer)
{
	this->index_buffer = index_buffer;
}

void vertex_array::generate_point_buffer()
{
	int dx = (width_ - 1) / (nx_ - 1);
	int dy = (height_ - 1) / (ny_ - 1);
	for (int i = 0; i < ny_; i++)
	{
		for (int j = 0; j < nx_; j++)
		{
			point_buffer.push_back(point(size_t(j * dx), size_t(i * dy),
				{uint8_t(255 * (i * dx + j * dy)/(width_ + height_)),0,0}));
		}
	}
}

void vertex_array::generate_index_buffer()
{
	bool flag = true;
	for (size_t i = 0; i < ny_ - 1; i++)
	{
		for (size_t j = 0; j < nx_ - 1; j++)
		{
			if (flag)
			{
				this->index_buffer.push_back(i * nx_ + j);
				//std::cout << *(this->index_buffer.end() - 1) << " ";
				this->index_buffer.push_back(i * nx_ + j + 1);
				//std::cout << *(this->index_buffer.end() - 1) << " ";
				this->index_buffer.push_back(i * nx_ + j + nx_ + 1);
				//std::cout << *(this->index_buffer.end() - 1) << " ";
			}
			else
			{
				this->index_buffer.push_back(i * nx_ + j);
				//std::cout << *(this->index_buffer.end() - 1) << " ";
				this->index_buffer.push_back(i * nx_ + j + nx_ + 1);
				//std::cout << *(this->index_buffer.end() - 1) << " ";
				this->index_buffer.push_back(i * nx_ + j + nx_);
				//std::cout << *(this->index_buffer.end() - 1) << " ";
			}
		}
		if (flag)
		{
			flag = false;
			i--;
		}
		else
		{
			flag = true;
		}
	}
}
