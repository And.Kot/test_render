#pragma once

#include "canvas.h"
#include "point.h"
#include <vector>

class line
{
public:
	line(point& p0, point& p1);

	void draw(canvas& buffer);
	static void fill(point& p0_, point& p1_, std::vector<point>& p_vector);

private:
	point p0_;
	point p1_;

	std::vector<point> p_vector;
};

