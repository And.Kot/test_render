#include <iostream>
#include "canvas.h"
#include <SDL.h>
#include "point.h"
#include "line.h"
#include "triangle.h"
#include "interpolated_triangle.h"
#include "vertex_array.h"
#include <ctime>
#include "my_gfx_program.h"

int main(int argc, char* args[])
{
	// task 01
	color green = { 0, 255, 0 };
	color blue = { 0, 0, 255 };
	color red = { 255, 0, 0 };
	color black = { 0, 0, 0 };
	color white = { 255, 255, 255 };
	color yellow = { 255, 255, 0 };

	canvas image;
	std::fill(image.begin(), image.end(), green);

	image.save_image("image.ppm");

	canvas image_loader;
	image_loader.load_image("image.ppm");
	image_loader.save_image("image2.ppm");

	// task 02
	point p1(10, 10, red);
	point p2(200, 200, blue);

	image.clear(green);
	p1.draw(image);
	image.save_image("image_02.ppm");

	// task 03
	line l(p2, p1);
	image.clear(green);
	l.draw(image);
	image.save_image("image_03.ppm");

	// task 04
	point pt1(12, 67, red), pt2(67, 12, green), pt3(100, 200, blue);
	triangle t(pt1, pt2, pt3);
	image.clear(green);
	t.draw(image);
	image.save_image("image_04.ppm");

	// task 05
	point ptc1(100, 10, red), ptc2(300, 120, blue), ptc3(10, 200, white);
	
	interpolated_triangle i_t(ptc1, ptc2, ptc3);
	
	image.clear(green);
	i_t.draw_edge(image);
	image.save_image("image_05.ppm");

	// task 05.1
	image.clear(green);
	i_t.draw(image);
	image.save_image("image_05.1.ppm");

	// task 06
	vertex_array v_arr(image_width, image_height, 10, 10);
	image.clear(green);
	double duration;
	std::clock_t start;
	start = std::clock();
	v_arr.draw(image);
	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << duration << std::endl;
	image.save_image("image_06.ppm");

	///////////////////////////////////////

	image.clear(green);
	
	point p_default(160, 120, black);
	point p_calculated(0, 0, red);
	p_default.draw(image);

	//vertex_array va_default(image_width, image_height, 10, 10);
	vertex_array va_calculated(image_width, image_height, 10, 10);
	std::vector<point> pa_default = va_calculated.get_point_buffer();
	std::vector<point> pa_calculated = va_calculated.get_point_buffer();

	my_gfx_program gfx_;

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window* window = SDL_CreateWindow("SDL_window", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, image_width, image_height,
		SDL_WINDOW_ALLOW_HIGHDPI);

	SDL_Renderer* renderer;
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	void* pixels = image.data();
	const int depth = sizeof(color) * 8;
	const int pitch = image_width * sizeof(color);
	const int rmask = 0x000000ff;
	const int gmask = 0x0000ff00;
	const int bmask = 0x00ff0000;
	const int amask = 0;

	float mouse_x{ 0 };
	float mouse_y{ 0 };
	float radius{ 40 }; // 20 pixels radius


	SDL_Event window_event;
	bool is_running = true;

	while (is_running)
	{
		if (SDL_PollEvent(&window_event) != 0)
		{
			if (window_event.type == SDL_QUIT)
			{
				is_running = false;
				break;
			}
			else if (window_event.type == SDL_MOUSEMOTION)
			{
				mouse_x = window_event.motion.x;
				mouse_y = window_event.motion.y;
			}
			/*else if (window_event.type == SDL_MOUSEWHEEL)
			{
				radius += window_event.wheel.y;
			}*/



			gfx_.set_uniforms({ static_cast<int>(mouse_x),static_cast<int>(mouse_y), 
				static_cast<int>(radius) });
			pa_calculated =  gfx_.calculate_array(pa_default);
			va_calculated.set_point_buffer(pa_calculated);
			
			image.clear(green);
			va_calculated.draw(image);
			/*p_calculated = gfx_.point_shader(p_default);

			image.clear(red);
			p_calculated.draw(image);*/
			
			pixels = image.data();

			SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(
				pixels, image_width, image_height, depth, pitch, rmask, gmask, bmask, amask);

			SDL_Texture* bitmapTex =
				SDL_CreateTextureFromSurface(renderer, bitmapSurface);

			SDL_FreeSurface(bitmapSurface);

			SDL_RenderClear(renderer);
			SDL_RenderCopy(renderer, bitmapTex, nullptr, nullptr);
			SDL_RenderPresent(renderer);

			SDL_DestroyTexture(bitmapTex);
		}


		std::cout << "x = " << mouse_x << "\t" << "y = " << mouse_y << std::endl;
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();


	system("pause");
	return 0;
}