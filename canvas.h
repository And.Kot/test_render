#pragma once

#include "color.h"

const size_t image_width  = 320;
const size_t image_height = 240;

const size_t pixel_quantity = image_width * image_height;

class canvas: public std::array<color, pixel_quantity>
{
public:
	void save_image(const std::string& file_name);
	void load_image(const std::string& file_name);
	void clear(color& c);
};

