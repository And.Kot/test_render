#pragma once

#include "triangle.h"

class interpolated_triangle
{
public:
	interpolated_triangle(point& p0, point& p1, point& p2);
	
	void draw_edge(canvas &buffer);
	void draw(canvas &buffer);

private:
	point p0_;
	point p1_;
	point p2_;

	std::vector<point> p_vector;
	std::vector<point> p_vector_edge_full;
	std::vector<point> p01_vector_edge;
	std::vector<point> p12_vector_edge;
	std::vector<point> p20_vector_edge;

	void sort_points();
	void fill();
	void fill_edge(point& p0, point& p1, std::vector<point>& p_vector_edge);
	
};

