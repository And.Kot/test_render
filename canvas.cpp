#include "canvas.h"

#include <fstream>
#include <iostream>

void canvas::save_image(const std::string& file_name)
{
	std::ofstream file;
	file.open(file_name, std::ios_base::binary);

	file << "P6\n" << image_width << ' ' << image_height << ' ' << 255 << '\n';

	for (auto it = this->begin(); it != this->end(); ++it)
	{
		file.write(reinterpret_cast<const char*>(&color(*it)), sizeof(color));
	}
	file.close();
}

void canvas::load_image(const std::string& file_name)
{
	std::ifstream file;

	file.open(file_name, std::ios_base::binary);

	for (auto it = this->begin(); it != this->end(); ++it)
	{
		file.read(reinterpret_cast<char*>(&color(*it)), sizeof(color));
	}
	file.close();
}

void canvas::clear(color& c)
{
	std::fill(this->begin(), this->end(), c);
}
