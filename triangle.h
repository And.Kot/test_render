#pragma once

#include "line.h"

class triangle
{
public:
	triangle(point& p0, point& p1, point& p2);

	void draw(canvas& buffer);
	static void fill(point& p0, point& p1, point& p2, std::vector<point>& p_vector);

private:
	point p0_;
	point p1_;
	point p2_;

	std::vector<point> p_vector;
};

