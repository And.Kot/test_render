#pragma once

#include "color.h"
#include "canvas.h"

class point : public color/*, public my_gfx_program*/
{
public:
	point();
	point(size_t x, size_t y, color col);

	void draw(canvas& buffer);
	size_t get_x();
	size_t get_y();
	void set_xy(size_t x, size_t y);

	friend bool operator ==(const point& left, const point& right);

private:
	size_t x_;
	size_t y_;
};

