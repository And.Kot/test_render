#include "my_gfx_program.h"
#include <cmath>

void my_gfx_program::set_uniforms(const uniforms& uniform_)
{
	mouse_x = uniform_.f0;
	mouse_y = uniform_.f1;
	mouse_r = uniform_.f2;
}

point my_gfx_program::point_shader(point& p)
{
	color c = { p.g, p.r, p.b };
    float x = p.get_x();
	float y = p.get_y();

	float dx = x - mouse_x;
	float dy = y - mouse_y;
    if (dx * dx + dy * dy < mouse_r * mouse_r && dx*dx > 0 && dy*dy >0)
    {
		x = static_cast<int>(x + 0.25 * (dx ));
		y = static_cast<int>(y + 0.25 * (dy ));
		c = {255,255,255 };
    }

	x < 0 ? x = 0 : x;
	x > image_width - 1 ? x = image_width - 1 : x;
	y < 0 ? y = 0 : y;
	y > image_height - 1 ? y = image_height - 1 : y;

    return point(x, y, c);
}

color my_gfx_program::pixel_shader(point& p)
{
    return color({0, 0, 0});
}

std::vector<point> my_gfx_program::calculate_array(std::vector<point>& p_array)
{
	std::vector<point> result;
	result.resize(p_array.size());
	for (size_t i = 0; i < p_array.size(); i++)
	{
		result.at(i) = point_shader(p_array.at(i));
	}
	return result;
}
