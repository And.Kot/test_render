#include "triangle.h"

triangle::triangle(point& p0, point& p1, point& p2):
	p0_(p0),
	p1_(p1),
	p2_(p2)
{
	line::fill(p0_, p1_, p_vector);
	line::fill(p1_, p2_, p_vector);
	line::fill(p2_, p0_, p_vector);
}

void triangle::draw(canvas& buffer)
{
	for (size_t i = 0; i < p_vector.size(); i++)
	{
		*(buffer.begin() + p_vector.at(i).get_x() + p_vector.at(i).get_y() * image_width)
			= color(this->p_vector.at(i));
	}
}

void triangle::fill(point& p0, point& p1, point& p2, std::vector<point>& p_vector)
{
	line::fill(p0, p1, p_vector);
	line::fill(p1, p2, p_vector);
	line::fill(p2, p0, p_vector);
}
